/* 
 * Cosc 345 Assignment
 * Team: Celocity
 * Cookin' Companion
 * 
 * appGui.js
 * Handles the Processing of Raw Input and the Drawing/arrangment of the App Content
 */

console.log("app.js loading");

var App = (function(){
    
    'use strict';
    var pub = {}, currList, scrollPos = 0, fps = 50, icons, images, iconURLs, data = [],  startList = "start_screen";
    //Porting Emulator's Easel Stage
    var stage;
    //List of Stage objects
    var renderList = [];
    
    /*
     * Data Structure for a list, which can be a recipe, or a menu.
     * id: the unique name identifier of the list
     * swipe: points to the list to switch to should the user swipe sideways
     * items: an array of item objects
     */
    function listObject(id, swipe, items){
        this.id = id;
        this.swipe = swipe;
        this.items = items;
    }
    
    /*
     * Object that represents the basic units of the App's content
     * text:  a string that serves as a displayed label and/or identifier
     * type: denoted what type of item it is
     * data1, data2: data fields whose contents depend on the type.
     */
    function itemObject(text, type, data1, data2){
        this.text = text;
        this.type = type;
        this.data1 = data1;
        this.data2 = data2;
    }
    
    /*
     * Runs the per-fram functions
     */
    function mainLoop(){
        //Emulator.drawRect(0,0,320,320,"#007C9A");
        var background = new createjs.Shape();
        background.graphics.beginFill("#007C9A").drawRect(0, 0, 320, 320);
        stage.addChild(background);
        stage.update();
        //getItems();
        if(currList !== undefined && checkImages()){
            var y = 5, i;
            for(i = 0; i < currList.items.length; i+=1){
                y = itemDisplay(currList.items[i], y);
            }
        }
    }
    
    /*
     * Loads in Images from their Urls and stores them in an Array
     */
    function loadImages(){
        if(currList === undefined){
            return;
        }
        var i, item, img;
        for(i = 0; i < currList.items.length; i+=1){
            item = currList.items[i];
            if(item.type === "Image" && images[i] === undefined){
                img = new Image();
                img.src = item.data1;
                images[i] = img;
            }
        }
    }
    
    pub.test = function(){
        return renderList;
    };
    
    /*
     * Preloads the Icon images and loads them into an Array.
     */
    function preloadIcons(){
        var i, img;
        for(i = 0; i < iconURLs.length; i+=1){
            img = new Image();
            img.src = iconURLs[i];
            icons[i] = img;
        }
    }
    
    /*
     * Ensures that images are loaded to prevent broken displaying
     */
    function checkImages(){
        var i;
        for(i = 0; i < currList.items.length; i+=1){
            if(currList.items[i].type === "Image" && images[i] === undefined){
                return false;
            }
        }
        for(i = 0; i < iconURLs.length; i+=1){
            if(icons[i] === undefined){
                return false;
            }
        }
        return true;
    }
    
    /*
     * Determine the height in pixels of a given item
     */
    function getItemHeight(item){
        switch(item.type){
            case 'Label':
                var height = icons[1].height, i = find(item);
                if(currList.items[i+1].type === "Toggle"){
                    height -= 10;
                }
                return height;
            case 'Image':
                return images[find(item)].height;
            case 'Skip':
                return icons[2].height-20;
            case 'TickAll':
                return icons[3].height-60;
            case 'Toggle':
                var height = icons[4].height, i = find(item);
                if(currList.items[i+1].type === "Toggle"){
                    height -=20;
                }
                return height;
            case 'Timer':
                return 75;
            default:
                return icons[0].height;
        }
    }
    
    /*
     * Publicly accessible method for requesting a new list to be assigned
     * to the accessible interface
     */
    function setMenu(id){
        var i = 0;
        for(i; i<data.length; i+=1){
            if(data[i].id === id){
                currList = data[i];
            }
        }
        loadImages();
        createRenderList();
    };
    
    function createRenderList(){
        if(!checkImages()){
            renderList = undefined;
            return;
        }
        renderList = [];
        var item, margin = 5, height = margin, i = 0, font = "22px arial";
        for(i; i<currList.items.length; i++){
            item = currList.items[i];
            var things, panel, text;
            var itemHeight = getItemHeight(item);
            var things = [];
            switch(item.type){
                case "Image":
                    panel = new createjs.Bitmap(images[find(item)]);
                    panel.x = margin;
                    panel.y = height;
                    height += (itemHeight + margin);
                    things[0] = panel;
                    break;
                case "Toggle":
                    panel = new createjs.Bitmap(icons[4]);
                    panel.x = margin+11;
                    panel.y = height;
                    height += (itemHeight + margin);
                    things[0] = panel;
                    text = new createjs.Text(item.text, font, "#000000");
                    text.x = margin+25;
                    text.y = height+50;
                    height += (itemHeight + margin);
                    things[1] = text;
                    break;
                case "TickAll":
                    panel = new createjs.Bitmap(icons[3]);
                    panel.x = margin+11;
                    panel.y = height;
                    height += (itemHeight + margin);
                    things[0] = panel;
                    text = new createjs.Text(item.text, font, "#000000");
                    text.x = margin+30;
                    text.y = height+30;
                    height += (itemHeight + margin);
                    things[1] = text;
                    break;
                case "Timer":
                    colour = "#FFFFFF";
                    panel = new createjs.Shape();
                    panel.graphics.beginFill(colour).drawRect(margin, height, 310, itemHeight);
                    things[0] = panel;
                    text = new createjs.Text(item.text, font, "#000000");
                    text.x = margin+10;
                    text.y = height+30;
                    things[1] = text;
                    colour = "#FF0000";
                    var w = 310-10;
                    var r = Math.abs(item.data2)/Math.abs(item.data1);
                    var bar = new createjs.Shape();
                    bar.graphics.beginFill(colour).drawRect(margin+5, height+45, w*r, 20);
                    height += itemHeight + margin;
                    things[2] = bar;
                    break;
                case "Label":
                    panel = new createjs.Bitmap(icons[1]);
                    panel.x = margin;
                    panel.y = height;
                    things[0] = panel;
                    if (item.data1 === "bold") {
                        font = "bold 30px arial";
                    }
                    text = new createjs.Text(item.text, font, "#000000");
                    text.x = margin + 10;
                    text.y = height + 40;
                    height += itemHeight + margin;
                    things[1] = text;
                    break;
                case "Skip":
                    panel = new createjs.Bitmap(icons[2]);
                    panel.x = margin;
                    panel.y = height;
                    things[0] = panel;
                    text = new createjs.Text(item.text, font, "#000000");
                    text.x = margin + 30;
                    text.y = height + 30;
                    things[1] = text;
                    height += itemHeight + margin;
                    break;
                case "Goto":
                    panel = new createjs.Bitmap(icons[0]);
                    panel.x = margin;
                    panel.y = height;
                    things[0] = panel;
                    text = new createjs.Text(item.text, font, "#000000");
                    text.x = margin + 30;
                    text.y = height + 60;
                    things[1] = text;
                    height += itemHeight + margin;
                    break;
                default:
                    panel = new createjs.Bitmap(icons[0]);
                    panel.x = margin;
                    panel.y = height;
                    things[0] = panel;
                    text = new createjs.Text(item.text, font, "#000000");
                    text.x = margin + 30;
                    text.y = height + 60;
                    things[1] = text;
                    height += itemHeight + margin;
                    break;
            }
            renderList[i] = things;
        }
    }
    
    /*
     * Handles the display of each item based on it's type and its data
     */
    function itemDisplay(item, height){
        var boxHeight, boxWidth = 310;
        var margin = 5;
        var colour;
        var font = "22px arial";
        var panel, text;
        boxHeight = getItemHeight(item);
        switch(item.type){
        case "Image":
            //Emulator.drawImage(images[find(item)], margin, scrollPos+height);
            panel = new createjs.Bitmap(images[find(item)]);
            panel.x = margin;
            panel.y = scrollPos+height;
            stage.addChild(panel);
            stage.update();
            height += boxHeight + margin;
            return height;
        case "Toggle":
            if(item.data1 === 1){
                //Emulator.drawImage(icons[5], margin+11, height+scrollPos);
                panel = new createjs.Bitmap(icons[5]);
                panel.x = margin+11;
                panel.y = height+scrollPos;
                stage.addChild(panel);
                //Emulator.drawText(margin+25, height+50+scrollPos, item.text, "#000000", font);
                text = new createjs.Text(item.text, font, "#000000");
                text.x = margin+25;
                text.y = height+50+scrollPos;
                stage.addChild(text);
                stage.update();
                height += boxHeight + margin;
                return height;
            }else{
                //Emulator.drawImage(icons[4], margin+11, height+scrollPos);
                panel = new createjs.Bitmap(icons[4]);
                panel.x = margin+11;
                panel.y = height+scrollPos;
                stage.addChild(panel);
                //Emulator.drawText(margin+25, height+50+scrollPos, item.text, "#000000", font);
                text = new createjs.Text(item.text, font, "#000000");
                text.x = margin+25;
                text.y = height+50+scrollPos;
                stage.addChild(text);
                stage.update();
                height += boxHeight + margin;
                return height;
            }
        case "TickAll":
            if(item.data1 === 1){
                //Emulator.drawImage(icons[6], margin, height+scrollPos);
                panel = new createjs.Bitmap(icons[6]);
                panel.x = margin;
                panel.y = height+scrollPos;
                stage.addChild(panel);
                //Emulator.drawText(margin+30, height+30+scrollPos, item.text, "#000000", font);
                text = new createjs.Text(item.text, font, "#000000");
                text.x = margin+30;
                text.y = height+30+scrollPos;
                stage.addChild(text);
                stage.update();
                height += boxHeight + margin;
                return height;
            }else{
                //Emulator.drawImage(icons[3], margin, height+scrollPos);
                panel = new createjs.Bitmap(icons[3]);
                panel.x = margin;
                panel.y = height+scrollPos;
                stage.addChild(panel);
                //Emulator.drawText(margin+30, height+30+scrollPos, item.text, "#000000", font);
                text = new createjs.Text(item.text, font, "#000000");
                text.x = margin+30;
                text.y = height+30+scrollPos;
                stage.addChild(text);
                stage.update();
                height += boxHeight + margin;
                return height;
            }
        case "Timer":
            colour = "#FFFFFF";
            //Emulator.drawRect(margin, height+scrollPos, boxWidth, boxHeight, colour);
            panel = new createjs.Shape();
            panel.graphics.beginFill(colour).drawRect(margin, height+scrollPos, boxWidth, boxHeight);
            stage.addChild(panel);
            //Emulator.drawText(margin+10, height+30+scrollPos, item.text, "#000000", font);
            text = new createjs.Text(item.text, font, "#000000");
            text.x = margin+10;
            text.y = height+30+scrollPos;
            stage.addChild(text);
            if(item.data2 < 0){
                colour = "#00FF00";
                item.data2 += (1/fps);
            }else{
                colour = "#FF0000";
            }
            var w = boxWidth-10;
            var r = Math.abs(item.data2)/Math.abs(item.data1);
            //Emulator.drawRect(margin+5, height+scrollPos+45, w*r, 20, colour);
            var bar = new createjs.Shape();
            bar.graphics.beginFill(colour).drawRect(margin+5, height+scrollPos+45, w*r, 20);
            stage.addChild(bar);
            stage.update();
            height += boxHeight + margin;
            return height;
        case "Label":
            //Emulator.drawImage(icons[1], margin, height+scrollPos);
            panel = new createjs.Bitmap(icons[1]);
            panel.x = margin;
            panel.y = height+scrollPos;
            stage.addChild(panel);
            if(item.data1 === "bold"){
                font = "bold 30px arial";
            }
            //Emulator.drawText(margin+10, height+40+scrollPos, item.text, "#000000", font);
            text = new createjs.Text(item.text, font, "#000000");
            text.x = margin+10;
            text.y = height+40+scrollPos;
            stage.addChild(text);
            stage.update();
            height += boxHeight + margin;
            return height;
        case "Skip":
            //Emulator.drawImage(icons[2], margin, height+scrollPos);
            panel = new createjs.Bitmap(icons[2]);
            panel.x = margin;
            panel.y = height+scrollPos;
            stage.addChild(panel);
            //Emulator.drawText(margin+30, height+30+scrollPos, item.text, "#000000", font);
            text = new createjs.Text(item.text, font, "#000000");
            text.x = margin+30;
            text.y = height+30+scrollPos;
            stage.addChild(text);
            stage.update();
            height += boxHeight + margin;
            return height;
        case "Goto":
            //Emulator.drawImage(icons[0], margin, height+scrollPos);
            panel = new createjs.Bitmap(icons[0]);
            panel.x = margin;
            panel.y = height+scrollPos;
            stage.addChild(panel);
            //Emulator.drawText(margin+30, height+60+scrollPos, item.text, "#000000", font);
            text = new createjs.Text(item.text, font, "#000000");
            text.x = margin+30;
            text.y = height+60+scrollPos;
            stage.addChild(text);
            stage.update();
            height += boxHeight + margin;
            return height;
        default:
            //Emulator.drawImage(icons[0], margin, height+scrollPos);
            panel = new createjs.Bitmap(icons[0]);
            panel.x = margin;
            panel.y = height+scrollPos;
            stage.addChild(panel);
            //Emulator.drawText(margin+10, height+50+scrollPos, item.text, "#000000", font);
            text = new createjs.Text(item.text, font, "#000000");
            text.x = margin+30;
            text.y = height+60+scrollPos;
            stage.addChild(text);
            stage.update();
            height += boxHeight + margin;
            return height;
        }
    }
    
    /*
     * Called when an Item is clicked on, determines and carries out the appropriate action.
     */
    function itemAction(item){
        var i;
        switch(item.type){
        case "Goto":
            setMenu(item.data1);
            images = [];
            scrollPos=0;
            return;
        case "Toggle":
            item.data1 = (1-item.data1);
            return;
        case "Timer":
            item.data2 = -item.data2;
            return;
        case "TickAll":
            item.data1 = (1-item.data1);
            var index = find(item), steps = parseInt(item.data2);
            for(i = index+1; i <= (index+steps); i+=1){
                if(currList.items[i].type === "Toggle"){
                    currList.items[i].data1 = (item.data1);
                }
            }
            return;
        case "Skip":
            var pos = 0, margin = 5;
            for(i = 0; i < parseInt(item.data1); i+=1){
                pos -= getItemHeight(currList.items[i]);
                pos -= margin;
            }
            scrollPos = pos;
            return;
        }
    }
    /*
     * finds the index of a given item based on its text field
     */
    function find(item){
        var i;
        for(i = 0; i < currList.items.length; i+=1){
            if(currList.items[i].text === item.text){
                return i;
            }
        }
    }
    
    /*
     * Determines which item was clicked on, if any when a click occurs.
     */
    function getClickedItem(x, y){
        y-=scrollPos;
        if(x < 5 || x > 315){
            return;
        }
        var i, margin = 5;
        var top = margin, bot, height;
        for(i = 0; i < currList.items.length; i+=1){
            height = getItemHeight(currList.items[i]);
            bot = top+height;
            if(y >= top && y <= bot){
                return currList.items[i];
            }
            top = bot+margin;
        }
        return null;
    }
    
    /*
     * Changes the current list when the user uses swipe navigation
     */
    function swipeNavigate(){
        var id = currList.swipe;
        if(id !== ""){
            scrollPos = 0;
            setMenu(id);
        }
    }
    
    /*
     * Parses XML data into appropriate data structures
     */
    function parseXml(xml){
        var lists = $(xml).find("list");
        $(lists).each(function(){
            var l = new listObject($(this).attr("id"), 
                $(this).find("swipe").text(),null);
            var items = $(this).find("item");
            var itemArray = [];
            $(items).each(function(){
                var i = new itemObject(
                        $(this).find("text").text(),
                        $(this).find("type").text(),
                        $(this).find("data").text(),
                        $(this).find("data2").text()
                                );
                itemArray.push(i);
            });
            l.items = itemArray;
            data.push(l);
        });
        currList = data[0];
        
        loadImages();
        createRenderList();
    }
    
    /*
     * Setup function that initialises variables, begins the main loop and handles the Swipefunctionality
     */
    pub.init = function(calls_in, stage_in, sounds_in){
        console.log("init running");
        stage = stage_in;
        $.ajax({
            url: "Recipes/recipes.xml", 
            type: "GET",
            dataType: "text",
            success: function(result){
                parseXml(result);
            }
        });
        icons = [];
        images = [];
        iconURLs = ["Images/Icons/David/button2.png", 
            "Images/Icons/karam/standardStep.png",
            "Images/Icons/David/skipBtn.png",
            "Images/Icons/David/subheadingBtn.png",
            "Images/Icons/David/descriptionBtn.png",
            "Images/Icons/David/selectedOverly.png",
            "Images/Icons/David/selectAllOverlay.png"];
        var touchScreen = $("#touchScreen")[0];
        //Emulator.config(320, 320, 0.5);
        preloadIcons();
        setInterval(mainLoop, 1000/fps);
//        var oldScroll, swipeStart = false;
//        $(function() {
//          $(touchScreen).swipe({
//            swipeStatus:function(event, phase, direction, distance, duration, fingerCount, fingerData) {
//                if(phase !== "end" && phase !== "cancel"){
//                    if(!swipeStart){
//                        oldScroll = scrollPos;
//                        swipeStart = true;
//                    }
//                }
//                if(direction === "down"){
//                  scrollPos = oldScroll+distance;
//                }
//                if(direction === "up"){
//                  scrollPos = oldScroll-distance;  
//                }
//                if(phase === "cancel"){
//                    scrollPos = oldScroll; 
//                }
//                if(phase === "end"){
//                    if(direction === "right" && distance >= 100){
//                        swipeNavigate();
//                    }else if(distance < 100){
//                        var i = getClickedItem(Emulator.clickState.x, Emulator.clickState.y);
//                        if(i !== null){
//                            itemAction(i);
//                        } 
//                    }
//                    swipeStart = false;
//                }
//            },
//            threshhold:50
//          });
//          
//        });
    };
    
    return pub;
    
}());