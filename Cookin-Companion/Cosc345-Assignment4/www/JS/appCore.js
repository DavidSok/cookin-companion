/* 
 * Cosc 345 Assignment
 * Team: Celocity
 * Cookin' Companion
 * 
 * appCore.js
 * The base level of the App involved with loading the App's content from XML files
 * and storing them in data structures for the App's other components to access.
 * 
 * The App's current list can be accessed with AppCore.items;
 * The current list can be set with AppCore.setMenu(id);
 * where id is the rquested list's id field.
 */

var AppCore = (function(){
    
    'use strict';
    var data = [], pub = {};
    
    /*
     * Data Structure for a list, which can be a recipe, or a menu.
     * id: the unique name identifier of the list
     * swipe: points to the list to switch to should the user swipe sideways
     * items: an array of item objects
     */
    function list(id, swipe, items){
        this.id = id;
        this.swipe = swipe;
        this.items = items;
    }
    
    /*
     * Object that represents the basic units of the App's content
     * text:  a string that serves as a displayed label and/or identifier
     * type: denoted what type of item it is
     * data1, data2: data fields whose contents depend on the type.
     */
    function item(text, type, data1, data2){
        this.text = text;
        this.type = type;
        this.data1 = data1;
        this.data2 = data2;
    }
    
    /*
     * Publicly accessible method for requesting a new list to be assigned
     * to the accessible interface
     */
    pub.setMenu = function(id){
        var i = 0;
        for(i; i<data.length; i+=1){
            if(data[i].id === id){
                pub.items = data[i];
            }
        }
    };
    
    /*
     * Parses XML data into appropriate data structures
     */
    function parseXml(xml){
        var lists = $(xml).find("list");
        $(lists).each(function(){
            var l = new list($(this).attr("id"), 
                $(this).find("swipe").text(),null);
            var items = $(this).find("item");
            var itemArray = [];
            $(items).each(function(){
                var i = new item(
                        $(this).find("text").text(),
                        $(this).find("type").text(),
                        $(this).find("data").text(),
                        $(this).find("data2").text()
                                );
                itemArray.push(i);
            });
            l.items = itemArray;
            data.push(l);
        });
        pub.items = data[0];
    }
    
    
    
    /*
     * Set up method for loading the XML data
     */
    pub.setup = function(){
        $.ajax({
            url: "Recipes/recipes.xml", 
            type: "GET",
            dataType: "text",
            success: function(result){
                parseXml(result);
            }
        });
       
    };
    
    return pub;
    
}());

$(document).ready(AppCore.setup);