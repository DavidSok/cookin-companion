/* 
 * Cosc 345 Assignment
 * Team: Celocity
 * Cookin' Companion
 * 
 * This script operates a smart watch emulator that runs in a web browser
 */

var Emulator = (function(){
    
    'use strict';
    var touchScreen, screen, screenX, screenY, ratio, pub = {};
    
    /*
     * Object for observing the position of the mouse and whether
     * it is clicking or not.
     * Should only be read.
     */
    function cState(){
        this.x = 0;
        this.y = 0;
        this.isClicking = false;
    }
    
    /*
     * Object for observing the thermometer inputs
     * Ambient refers to a air temperature, Skin refers to the user's body temp.
     * Shouls only be read.
     */
    function tState(){
        this.ambient = 0;
        this.skin = 0;
    }
    
    /*
     * Object for observing the accelerometer
     * Measured on the 3 cartesian Axes
     * Shoud only be read.
     */
    function aState(){
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    
    /*
     * Object for observing the Orientation of the Smart Watch
     * Should only be read;
     */
    function oState(){
        this.compass = 0;
        this.roll = 0;
        this.yaw = 0;
        this.pitch = 0;
    }
    
    /* Public Method for drawing rectangles to the screen
     * 
     * int x: the x coord of the top left corner
     * int y: the y coord of the top left corner
     * int width: the width of the rect
     * int height: the height of the rect
     * string colour: the hex code of the colour to use e.g. "#ffffff"
     * boolean filled: Optional, true draws a filled rect, false is stroke only, defaults to true
     */
    pub.drawRect = function(x, y, width, height, colour, filled){
        screen.fillStyle = colour;
        if(filled === true || filled === undefined){
            screen.fillRect(x, y, width, height);
        }else{
            screen.strokeRect(x, y, width, height);
        }
    };
    
    /*Public method for drawing arc/circles to the screen
     * 
     * int x: the x coord of the arc's centre
     * int y: the y coord of the arc's centre
     * int radius: the radius of the arc
     * int startAngle: the angle at which the arc starts, 0 is right and progresses clockwise
     * int endAngle: tthe angle at which the arc ends, 0 is right and progresses clockwise
     * string colour: the hex code of the colour to use e.g. "#ffffff"
     * boolean filled: Optional, true draws a filled rect, false is stroke only, defaults to true
     */
    pub.drawArc = function(x, y, radius, startAngle, endAngle, colour, filled){
        screen.fillStyle = colour;
        screen.beginPath();
        screen.arc(x, y, radius, startAngle, endAngle);
        if(filled === true || filled === undefined){
            screen.fill();
        }else{
            screen.stroke();
        }
    };
    
   /* Public method for drawing Images
    * 
    * Image Element img: the image to draw
    * int x: the x coord of the top left corner
    * int y: the y coord of the top left corner
    */
    pub.drawImage = function(img, x, y){
        screen.drawImage(img, x, y);
    };
    
    /* Public method for writing text to the screen
     * 
     * int x: the x coord of the bottom left of the text
     * int y: the y coord of the bottom left of the text
     * string text: the string to write
     * string colour: the hex code of the colour to use e.g. "#ffffff"
     * string font: the font and font style to use e.g. "italic small-caps bold 12px arial"
     */
    pub.drawText = function(x, y, text, colour, font){
        screen.fillStyle = colour;
        if(font !== undefined){
            screen.font = font;
        }
        screen.fillText(text, x, y);
    };
    
    
    /* Public Method for controlling the emulator's vibrator
     * 
     * return true if the vibrator is now on, false if it's now off
     */
    pub.toggleVibration = function(on){
        if(on === undefined){
            var state = $("#vibrate").text();
            if(state === "off"){
                $("#vibrate").text("on");
                return true;
            }else{
                $("#vibrate").text("off");
                return false;
            }
        }else{
            if(on === true){
                $("#vibrate").text("on");
                return true;
            }else{
                $("#vibrate").text("off");
                return false;
            }
        }
    };
    
    /* Public method for setting up the emulator
     * 
     * int x: the width of the main screen in pixels
     * int y: the height of the main screen in pixels
     * int r: the size of the small screen as a decimal i.e. 0.5 is 50% the size
     */
    pub.config = function(x, y, r){
        $(touchScreen).attr("width", x);
        $(touchScreen).attr("height", y);
        var ss = $("#smallScreen");
        $(ss).attr("width", (x*r));
        $(ss).attr("height", (y*r));
        screenX = x;
        screenY = y;
        ratio = r;
        var w = $("#centre").width();
        var margin = (w-x)/2;
        $("#touchScreen").css("margin-left", margin);
        $("#touchScreen").css("margin-top", "20px");
        
    };
    
    /*
     * Handles the updating of the emulator's I/O interface
     */
    function mainloop(){
        var w = $("#centre").width();
        var margin = (w-screenX)/2;
        $("#touchScreen").css("margin-left", margin);
        $("#touchScreen").css("margin-top", "20px");
        setDataState();
        updateLabels();
        var ss = $("#smallScreen");
        var smallS = ss[0].getContext("2d");
        var img = document.createElement("img");
        var s = $("#touchScreen")[0];
        img.src = s.toDataURL("image/png");
        smallS.drawImage(img, 0, 0, screenX*ratio, screenY*ratio);
    }
    
    /*
     * Sets emulator inputs from web page inputs
     */
    function setDataState(){
        pub.tempState.ambient = $("#amTemp").val();
        pub.tempState.skin = $("#skTemp").val();
        pub.accState.x = $("#accX").val();
        pub.accState.y = $("#accY").val();
        pub.accState.z = $("#accZ").val();
        pub.orienState.compass = $("#compass").val();
        pub.orienState.roll = $("#roll").val();
        pub.orienState.yaw = $("#yaw").val();
        pub.orienState.pitch = $("#pitch").val();
    }
    
    /*
     * Manages misc labels and handles the overflow of the angular inputs.
     */
    function updateLabels(){
        $("#accXv").text(pub.accState.x);
        $("#accYv").text(pub.accState.y);
        $("#accZv").text(pub.accState.z);
        var o, i;
        var ids = ["#compass", "#roll", "#yaw", "#pitch"];
        for(i = 0; i < ids.length; i+=1){
            o = $(ids[i]).val();
            if(o === ""){
                continue;
            }
            o = parseInt(o);
            while(o > 180 || o < -180){
                if(o <= -180){
                    o += 360;
                }else if(o > 180){
                    o -= 360;
                }
            }
            $(ids[i]).val(o);
        }
    }
    
    /*
     * Tracks Mouse movement on the Main screen
     */
    function getCoords(e){
        var offset = $(this).offset();
        var relX = Math.round(e.pageX - offset.left);
        var relY = Math.round(e.pageY - offset.top);
        $("#xcoord").text(relX);
        pub.clickState.x = relX;
        $("#ycoord").text(relY);
        pub.clickState.y = relY;
    }
    
    /* 
     * Setup function that initialises the I/O interface objects and begine the main loop 
     */
    pub.setup = function(){
        touchScreen = $("#touchScreen");
        screen = touchScreen[0].getContext("2d");
        pub.clickState = new cState();
        pub.tempState = new tState();
        pub.accState = new aState();
        pub.orienState = new oState();
        setInterval(mainloop, 20);
        touchScreen.mousemove(getCoords);
        touchScreen.mousedown(function(){
            $("#click").text("true");
            pub.clickState.isClicking = true;
        });
        touchScreen.mouseup(function(){
            $("#click").text("false");
            pub.clickState.isClicking = false;
        });   
    };

    return pub;
}());

$(document).ready(Emulator.setup);