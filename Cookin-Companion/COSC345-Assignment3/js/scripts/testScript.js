/* 
 * A simple Test App to test some of the Emulator's functions that our App does not utilise
 */



var TestApp = (function(){
    
    var pub = {};
    
    function mainloop(){
        Emulator.drawRect(0, 0, 320, 320, "#000000");
        Emulator.drawArc(160, 160, 100, 0, 2*Math.PI, "#FF0000");
        if(Emulator.clickState.isClicking){
            Emulator.toggleVibration(true);
        }else{
            Emulator.toggleVibration(false);
        }
        var angle = (Emulator.orienState.compass)/180 * Math.PI;
        var x = 160 - 100*Math.sin(angle), y = 160 - 100*Math.cos(angle);
        Emulator.drawArc(x, y, 20, 0, 2*Math.PI, "#00FF00");
        Emulator.drawText(x-8, y+7, "N", "#000", "25px bold arial");
        
        
    }
    
    pub.setup = function(){
        Emulator.config(320, 320, 0.5);
        setInterval(mainloop, 40);
    };
    
    return pub;
    
})();

$(document).ready(TestApp.setup);