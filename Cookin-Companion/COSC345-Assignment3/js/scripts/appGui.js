/* 
 * Cosc 345 Assignment
 * Team: Celocity
 * Cookin' Companion
 * 
 * appGui.js
 * Handles the Processing of Raw Input and the Drawing/arrangment of the App Content
 */

var AppGui = (function(){
    
    'use strict';
    var pub = {}, list, scrollPos = 0, fps = 50, icons, images, iconURLs;
    
    /*
     * Runs the per-fram functions
     */
    function mainLoop(){
        Emulator.drawRect(0,0,320,320,"#007C9A");
        getItems();
        if(list !== undefined && checkImages()){
            var y = 5, i;
            for(i = 0; i < list.items.length; i+=1){
                y = itemDisplay(list.items[i], y);
            }
        }
    }
    
    /*
     * gets the AppCore's list from its interface
     */
    function getItems(){
        list = AppCore.items;
        loadImages();
    }
    
    /*
     * Loads in Images from their Urls and stores them in an Array
     */
    function loadImages(){
        if(list === undefined){
            return;
        }
        var i, item, img;
        for(i = 0; i < list.items.length; i+=1){
            item = list.items[i];
            if(item.type === "Image" && images[i] === undefined){
                img = new Image();
                img.src = item.data1;
                console.log(item.data1);
                images[i] = img;
            }
            
        }
    }
    
    /*
     * Preloads the Icon images and loads them into an Array.
     */
    function preloadIcons(){
        var i, img;
        for(i = 0; i < iconURLs.length; i+=1){
            img = new Image();
            img.src = iconURLs[i];
            icons[i] = img;
        }
    }
    
    /*
     * Ensures that images are loaded to prevent broken displaying
     */
    function checkImages(){
        var i;
        for(i = 0; i < list.items.length; i+=1){
            if(list.items[i].type === "Image" && images[i] === undefined){
                return false;
            }
        }
        for(i = 0; i < iconURLs.length; i+=1){
            if(icons[i] === undefined){
                return false;
            }
        }
        return true;
    }
    
    /*
     * Determine the height in pixels of a given item
     */
    function getItemHeight(item){
        switch(item.type){
            case 'Label':
                var height = icons[1].height, i = find(item);
                if(list.items[i+1].type === "Toggle"){
                    height -= 10;
                }
                return height;
            case 'Image':
                return images[find(item)].height;
            case 'Skip':
                return icons[2].height-20;
            case 'TickAll':
                return icons[3].height-60;
            case 'Toggle':
                var height = icons[4].height, i = find(item);
                if(list.items[i+1].type === "Toggle"){
                    height -=20;
                }
                return height;
            case 'Timer':
                return 75;
            default:
                return icons[0].height;
        }
    }
    
    /*
     * Handles the display of each item based on it's type and its data
     */
    function itemDisplay(item, height){
        var boxHeight, boxWidth = 310;
        var margin = 5;
        var colour;
        var font = "22px arial";
        boxHeight = getItemHeight(item);
        switch(item.type){
        case "Image":
            Emulator.drawImage(images[find(item)], margin, scrollPos+height);
            height += boxHeight + margin;
            return height;
        case "Toggle":
            if(item.data1 === 1){
                Emulator.drawImage(icons[5], margin+11, height+scrollPos);
                Emulator.drawText(margin+25, height+50+scrollPos, item.text, "#000000", font);
                height += boxHeight + margin;
                return height;
            }else{
                Emulator.drawImage(icons[4], margin+11, height+scrollPos);
                Emulator.drawText(margin+25, height+50+scrollPos, item.text, "#000000", font);
                height += boxHeight + margin;
                return height;
            }
        case "TickAll":
            if(item.data1 === 1){
                Emulator.drawImage(icons[6], margin, height+scrollPos);
                Emulator.drawText(margin+30, height+30+scrollPos, item.text, "#000000", font);
                height += boxHeight + margin;
                return height;
            }else{
                Emulator.drawImage(icons[3], margin, height+scrollPos);
                Emulator.drawText(margin+30, height+30+scrollPos, item.text, "#000000", font);
                height += boxHeight + margin;
                return height;
            }
        case "Timer":
            colour = "#FFFFFF";
            Emulator.drawRect(margin, height+scrollPos, boxWidth, boxHeight, colour);
            Emulator.drawText(margin+10, height+30+scrollPos, item.text, "#000000", font);
            if(item.data2 < 0){
                colour = "#00FF00";
                item.data2 += (1/fps);
            }else{
                colour = "#FF0000";
            }
            var w = boxWidth-10;
            var r = Math.abs(item.data2)/Math.abs(item.data1);
            Emulator.drawRect(margin+5, height+scrollPos+45, w*r, 20, colour);
            height += boxHeight + margin;
            return height;
        case "Label":
            Emulator.drawImage(icons[1], margin, height+scrollPos);
            if(item.data1 === "bold"){
                font = "bold 30px arial";
            }
            Emulator.drawText(margin+10, height+40+scrollPos, item.text, "#000000", font);
            height += boxHeight + margin;
            return height;
        case "Skip":
            Emulator.drawImage(icons[2], margin, height+scrollPos);
            Emulator.drawText(margin+30, height+30+scrollPos, item.text, "#000000", font);
            height += boxHeight + margin;
            return height;
        case "Goto":
            Emulator.drawImage(icons[0], margin, height+scrollPos);
            Emulator.drawText(margin+30, height+60+scrollPos, item.text, "#000000", font);
            height += boxHeight + margin;
            return height;
        default:
            Emulator.drawImage(icons[0], margin, height+scrollPos);
            Emulator.drawText(margin+10, height+50+scrollPos, item.text, "#000000", font);
            height += boxHeight + margin;
            return height;
        }
    }
    
    /*
     * Called when an Item is clicked on, determines and carries out the appropriate action.
     */
    function itemAction(item){
        var i;
        switch(item.type){
        case "Goto":
            AppCore.setMenu(item.data1);
            images = [];
            scrollPos=0;
            return;
        case "Toggle":
            item.data1 = (1-item.data1);
            return;
        case "Timer":
            item.data2 = -item.data2;
            return;
        case "TickAll":
            item.data1 = (1-item.data1);
            var index = find(item), steps = parseInt(item.data2);
            for(i = index+1; i <= (index+steps); i+=1){
                if(list.items[i].type === "Toggle"){
                    list.items[i].data1 = (item.data1);
                }
            }
            return;
        case "Skip":
            var pos = 0, margin = 5;
            for(i = 0; i < parseInt(item.data1); i+=1){
                pos -= getItemHeight(list.items[i]);
                pos -= margin;
            }
            scrollPos = pos;
            return;
        }
    }
    /*
     * finds the index of a given item based on its text field
     */
    function find(item){
        var i;
        for(i = 0; i < list.items.length; i+=1){
            if(list.items[i].text === item.text){
                return i;
            }
        }
    }
    
    /*
     * Determines which item was clicked on, if any when a click occurs.
     */
    function getClickedItem(x, y){
        y-=scrollPos;
        if(x < 5 || x > 315){
            return;
        }
        var i, margin = 5;
        var top = margin, bot, height;
        for(i = 0; i < list.items.length; i+=1){
            height = getItemHeight(list.items[i]);
            bot = top+height;
            if(y >= top && y <= bot){
                return list.items[i];
            }
            top = bot+margin;
        }
        return null;
    }
    
    /*
     * Changes the current list when the user uses swipe navigation
     */
    function swipeNavigate(){
        var id = list.swipe;
        if(id !== ""){
            scrollPos = 0;
            AppCore.setMenu(id);
        }
    }
    
    /*
     * Setup function that initialises variables, begins the main loop and handles the Swipefunctionality
     */
    pub.setup = function(){
        icons = [];
        images = [];
        iconURLs = ["Images/Icons/David/button2.png", 
            "Images/Icons/karam/standardStep.png",
            "Images/Icons/David/skipBtn.png",
            "Images/Icons/David/subheadingBtn.png",
            "Images/Icons/David/descriptionBtn.png",
            "Images/Icons/David/selectedOverly.png",
            "Images/Icons/David/selectAllOverlay.png"];
        var touchScreen = $("#touchScreen")[0];
        Emulator.config(320, 320, 0.5);
        preloadIcons();
        setInterval(mainLoop, 1000/fps);
        var oldScroll, swipeStart = false;
        $(function() {
          $(touchScreen).swipe({
            swipeStatus:function(event, phase, direction, distance, duration, fingerCount, fingerData) {
                if(phase !== "end" && phase !== "cancel"){
                    if(!swipeStart){
                        oldScroll = scrollPos;
                        swipeStart = true;
                    }
                }
                if(direction === "down"){
                  scrollPos = oldScroll+distance;
                }
                if(direction === "up"){
                  scrollPos = oldScroll-distance;  
                }
                if(phase === "cancel"){
                    scrollPos = oldScroll; 
                }
                if(phase === "end"){
                    if(direction === "right" && distance >= 100){
                        swipeNavigate();
                    }else if(distance < 100){
                        var i = getClickedItem(Emulator.clickState.x, Emulator.clickState.y);
                        if(i !== null){
                            itemAction(i);
                        } 
                    }
                    swipeStart = false;
                }
            },
            threshhold:50
          });
          
        });
    };
    
    return pub;
    
}());

$(document).ready(AppGui.setup);